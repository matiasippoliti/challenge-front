const div = document.getElementById("cards-container");
const total = document.getElementById("candidates");
const newest = document.getElementById("newest");
const oldest = document.getElementById("oldest");
const search = document.getElementById("search");

fetch("https://www.mockachino.com/a71b232c-218e-4d/users")
    .then(res => res.json())
    .then(data => {

        const users = data.results;

        users.forEach(user => {
            const {picture,name,location,email, registered} = user;
            const container = document.createElement("div");
            container.className = "card";

            container.innerHTML = `
              <img
                src="${picture.large}"
              />
              <div class="card-info">
                <h3 class="name">${name.first} ${name.last}</h3>
                <p>
                    <small>${email}</small>
                </p>
                <i class="fas fa-map-marker-alt">
                    <small class="location">${location.city} ${location.state}</small>
                </i>
              </div>
              <hr class="line"/>
              <div class="favorite">
                    <i id="icon-clock" class="far fa-clock"><span id="date">${registered.age}Y ago</span></i>

                    <i id="heart" class="far fa-heart"></i>    
              </div>`;


            div.appendChild(container);
        })

        total.textContent = `${data.info.results} candidates`;
    });